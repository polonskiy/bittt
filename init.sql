create table if not exists peers (
	hash text not null,
	id text not null,
	ip text not null,
	port integer not null,
	seed integer not null,
	upd integer not null,
	primary key (hash, id)
);

create index upd on peers (upd);
