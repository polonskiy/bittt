<?php

$ttl = 60;
$interval = 20*60;
$db = __DIR__.'/data/peers.db';

$stopped = isset($_GET['event']) && $_GET['event'] == 'stopped';
$id = empty($_GET['peer_id']) ? '' : bin2hex($_GET['peer_id']);
$hash = empty($_GET['info_hash']) ? '' : bin2hex($_GET['info_hash']);
$ip = $_SERVER['REMOTE_ADDR'];
$port = intval(empty($_GET['port']) ? '' : $_GET['port']);
$seed = intval(isset($_GET['left']) && $_GET['left'] == '0');
$want = intval(empty($_GET['numwant']) ? 50 : $_GET['numwant']);

$db = new PDO("sqlite:$db");

if ($stopped) {
	$db->query("delete from peers where id = '$id' and hash = '$hash'");
	die;
}

$t = time();
$db->query("replace into peers values ('$hash', '$id', '$ip', $port, $seed, $t)");

$upd = $t - ($interval + $ttl);
$db->query("delete from peers where upd < $upd");

$complete = $db->query("
	select count(hash) from peers where hash = '$hash' and seed = 1
")->fetchColumn();

$incomplete = $db->query("
	select count(hash) from peers where hash = '$hash' and seed = 0
")->fetchColumn();

$stmt = $db->query("
	select id, ip, port from peers where hash = '$hash' and id != '$id'
	order by random() limit $want
");
$peers = array();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$row['peer id'] = pack('H*', $row['id']);
	unset($row['id']);
	$peers[] = $row;
}

echo bencode(compact('peers', 'interval', 'complete', 'incomplete'));

function bencode($val) {
	if (is_numeric($val)) return 'i'.$val.'e';
	if (!is_array($val)) return strlen($val).':'.$val;
	$enc = '';
	if ($val === array_values($val)) {
		foreach ($val as $v) $enc .= bencode($v);
		return 'l'.$enc.'e';
	}
	foreach ($val as $k => $v) $enc .= bencode($k).bencode($v);
	return 'd'.$enc.'e';
}
