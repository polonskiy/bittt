# Bittt

BitTorrent tracker

## Install:

### setup database

	$ sqlite3 data/peers.db < init.sql

### set permissions

	$ chmod a=rwX -R data/

## Usage

You can use `http://host.com/bittt-path/` or `http://host.com/bittt-path/announce` as tracker url.
